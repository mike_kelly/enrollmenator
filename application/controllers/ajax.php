<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
	{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


function index()
		{
		//pull in all post data and sanitize
		$this->form_validation->set_rules('Utility', 'Utility', 'trim|xss_clean');
		$this->form_validation->set_rules('AccType', 'AccType', 'trim|xss_clean');
		$this->form_validation->set_rules('DocType', 'DocType', 'trim|xss_clean');
		$this->form_validation->set_rules('populated', 'populated', 'trim|xss_clean');
		$this->form_validation->set_rules('AccNumber', 'AccNumber', 'trim|xss_clean');
		$this->form_validation->set_rules('MeterNumber', 'MeterNumber', 'trim|xss_clean');
		$this->form_validation->set_rules('Email', 'Email', 'trim|xss_clean');
		$this->form_validation->set_rules('Phone', 'Phone', 'trim|xss_clean');
		$this->form_validation->set_rules('Fax', 'Fax', 'trim|xss_clean');
		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|xss_clean');
		$this->form_validation->set_rules('LastName', 'LastName', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvName', 'AccNumber', 'trim|xss_clean');
		$this->form_validation->set_rules('CompName', 'CompNumber', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvStrNumber', 'SrvStrNumber', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvStrName', 'SrvStrName', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvCity', 'SrvCity', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvState', 'SrvState', 'trim|xss_clean');
		$this->form_validation->set_rules('SrvZip', 'SrvZip', 'trim|xss_clean');
		$this->form_validation->set_rules('MailStreet', 'MailStreet', 'trim|xss_clean');
		$this->form_validation->set_rules('MailCity', 'MailCity', 'trim|xss_clean');
		$this->form_validation->set_rules('MailState', 'MailState', 'trim|xss_clean');
		$this->form_validation->set_rules('MailZip', 'MailZip', 'trim|xss_clean');
		
		//plug into the database model
		$insertResults = $this->contract_model->insert_record($this->input->post());
		
		// return something to figure out how to deal with errors
		// if($insertResults > 0){Return TRUE;}
		// else{Return FALSE;}
		}
	
function datapull()
	{
	
	//pull on entries in the contracts database
	$results = $this->contract_model->get_records($this->uri->segment(3));
	
	//get the count and some key variables to send to the ajax-view for display
	$resultCount = count($results);
	//the pdf generating function will actually do the big database pull for all the 
	//information for the documents
	
	$data = array(
		'count'		=>	$resultCount,
		'records'	=>	$results
		);
	//load a view that will be returned on an ajax request
	$this->load->view('ajax-view', $data);
	}
	
	}