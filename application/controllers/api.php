<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
	{
	public function index()
		{
		show_error('Sorry you are not Authorized to view this page!', 403);
		}
	
	function proginfo_get()
		{
		//This situation covers nothing entered after proginfo_get/*
		//or explicitly state a format e.g. xml or html / defaults to json
		//proginfo_get/$format
		if($this->uri->segment(3) != 'consumers' and $this->uri->segment(3) != 'dte')
			{
			$returnedData = $this->api_model->allContractVerbage();
			$data = array(
				'code'		=>	200,
				'data'		=>	$returnedData,
				'format'	=>	$this->uri->segment(3),
				'counter'	=>	0
				);
			$this->response($data);
			}
		//This elseif covers when a matching utility is entered but an accType is not specified
		//proginfo_get/$utility/$format
		elseif($this->uri->segment(4) != 'residential' and $this->uri->segment(4) != 'small' and $this->uri->segment(4) != 'large')
			{
			//echo ucfirst($this->uri->segment(3));
			
			$returnedData = $this->api_model->allContractVerbage(ucfirst($this->uri->segment(3)));
			$data = array(
				'code'		=>	200,
				'data'		=>	$returnedData,
				'format'	=>	$this->uri->segment(4),
				'counter'	=>	0
				);
			$this->response($data);
			}
		else
			{
			//echo ucfirst($this->uri->segment(3));
			$returnedData = $this->api_model->allContractVerbage(ucfirst($this->uri->segment(3)),$this->uri->segment(4));
			$data = array(
				'code'		=>	200,
				'data'		=>	$returnedData,
				'format'	=>	$this->uri->segment(5),
				'counter'	=>	0
				);
			$this->response($data);
			}
		}
	
	function proginfo_put()
		{
		$this->form_validation->set_rules('testText', 'testText', 'required');
		if ($this->form_validation->run() == FALSE)
			{
			$this->load->view('api-view-test');
			}
		else
			{
			
			$data['text'] = $this->input->post('testText');
			
			$returnedData = $this->api_model->test($data);
			if($returnedData > 0)
				{
				$data = $this->api_model->test(10,'retrieve');
				$string = $data->result_array();
				//print_r($string);
				echo nl2br($string[7]['text']);
				}
				
			else{echo 'Failure';}
			}
		/*
		
		$array = $returnedData->result_array();
		
		echo htmlspecialchars($array[0]['ProgramTerms']);
		*/
		}
	
	function response($response)
		{
		//if there is a response
		if(isset($response) && $response['code'] == 200)
			{
			if(isset($response['data']))
				{
				switch($response['format'])
					{
					//Encodes and outputs in xml
					case 'xml':
						$this->load->dbutil();
						$xmlOutput = $this->dbutil->xml_from_result($response['data']);
						$this->output
							->set_content_type('text/xml')
							->set_output($xmlOutput);
						break;
					
					//Encodes and outputs a html table
					case 'html':
						//send data array to a view()
						//create an html table out of the data array
						//print_r($response['data']->result_array());
						$this->load->view('api-view',$response);
						break;
					
					//Encodes and outputs in json - This is the default action
					default:
						$this->output
							->set_content_type('application/json')
							->set_output(json_encode($response['data']->result_array()));
					}
				}
			//send appropriate response code 200, 201, ect...
			}
		else
			{
			//Generic error - requested object not found
			show_404();
			}
		}
	}