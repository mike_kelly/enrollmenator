<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contracts extends CI_Controller
	{
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
		{
		//Set base variables
		$data = array(
			'title' 	=> 	"Program Contracts",
			'script'	=>	"contracts.js",
			'sessionID'	=>	md5($this->session->userdata('session_id'))
			);
		
		$programInfo = $this->contract_model->program_info();
		
		
		$consumersObject1 = new DateTime($programInfo[0]['Con_Enroll_Month']);
		$consumersObject2 = new DateTime($programInfo[0]['Con_Enroll_Month']);
		$consumersObject3 = new DateTime($programInfo[0]['Con_Enroll_Month']);
		$consumersObject4 = new DateTime($programInfo[0]['Con_Enroll_Month']);
		$consumersObject5 = new DateTime($programInfo[0]['Con_Enroll_Month']);
		
		$consumersObject1->modify('-2 months');
		$consumersObject2->modify('-1 month');
		$consumersObject4->modify('+1 month');
		$consumersObject5->modify('+2 months');		
		
		$dteObject1 = new DateTime($programInfo[0]['DTE_Enroll_Month']);
		$dteObject2 = new DateTime($programInfo[0]['DTE_Enroll_Month']);
		$dteObject3 = new DateTime($programInfo[0]['DTE_Enroll_Month']);
		$dteObject4 = new DateTime($programInfo[0]['DTE_Enroll_Month']);
		$dteObject5 = new DateTime($programInfo[0]['DTE_Enroll_Month']);

		$dteObject1->modify('-2 months');
		$dteObject2->modify('-1 month');
		$dteObject4->modify('+1 month');
		$dteObject5->modify('+2 months');
		
		$data['ConsumersdateOptions'] = array(
		'<option value="'.$consumersObject1->format('Y-m-d').'">'.$consumersObject1->format('F').'</option>',
		'<option value="'.$consumersObject2->format('Y-m-d').'">'.$consumersObject2->format('F').'</option>',
		'<option value="'.$consumersObject3->format('Y-m-d').'" selected="selected">'.$consumersObject3->format('F').'</option>',
		'<option value="'.$consumersObject4->format('Y-m-d').'">'.$consumersObject4->format('F').'</option>',
		'<option value="'.$consumersObject5->format('Y-m-d').'">'.$consumersObject5->format('F').'</option>'
			);
			
		$data['DTEdateOptions'] = array(
		'<option value="'.$dteObject1->format('Y-m-d').'">'.$dteObject1->format('F').'</option>',
		'<option value="'.$dteObject2->format('Y-m-d').'">'.$dteObject2->format('F').'</option>',
		'<option value="'.$dteObject3->format('Y-m-d').'" selected="selected">'.$dteObject3->format('F').'</option>',
		'<option value="'.$dteObject4->format('Y-m-d').'">'.$dteObject4->format('F').'</option>',
		'<option value="'.$dteObject5->format('Y-m-d').'">'.$dteObject5->format('F').'</option>'
			);
		
		$this->load->view('template/header', $data);
		$this->load->view('contract-view', $data);
		$this->load->view('template/footer');
		}
	
	function pdf_generate($array = NULL)
		{
		require_once(FCPATH.'dompdf/dompdf_config.inc.php');
		
				//returns correctly formatted telephone and fax numbers
				function teleformat($numberString)
					{
					$stringStripped = ereg_replace("[^0-9]", "", $numberString );
					$stringArray = str_split($stringStripped);
			
					if(count($stringArray) == 11)
						{$stringFormatted = $stringArray[0].'('.$stringArray[1].$stringArray[2].$stringArray[3].')'.$stringArray[4].$stringArray[5].$stringArray[6].'-'.$stringArray[7].$stringArray[8].$stringArray[9].$stringArray[10];}
					elseif(count($stringArray) == 10)
						{$stringFormatted = '('.$stringArray[0].$stringArray[1].$stringArray[2].')'.$stringArray[3].$stringArray[4].$stringArray[5].'-'.$stringArray[6].$stringArray[7].$stringArray[8].$stringArray[9];}
					elseif(count($stringArray) == 7)
						{$stringFormatted = $stringArray[0].$stringArray[1].$stringArray[2].'-'.$stringArray[3].$stringArray[4].$stringArray[5].$stringArray[6];}
					else{$stringFormatted = $stringStripped;}
			
					RETURN $stringFormatted;
					}
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////// Collecting ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		//terms sent to the sql query
		$contractSearch = array(
			'Utility'		=>	$_POST['Utility'],
			'ContractType'	=>	$_POST['AccType']
			);
		
		//$programInfo is the results of much querying, contains action dates / category prices / and so forth specific to the customer choice program
		//$programInfo = $this->contract_model->program_info();
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////// Formatting ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		//$date is the define enrollment date by the user the contract expiration date is calculated by adding 1 year and changing the month/day to 3-31
		$date = $this->input->post('Date');
		
		list($year, $month, $day) = split('[-]', $date);
		
		//Creates date objects and correctly formats them for use in the pdf text
		try{
			//EnrollDate remains the same as the selected date by the user
			$EnrollDate_Object = new DateTime($date);
			
			$ExpireDate_Object = new DateTime($year.'-03-31');
			$ExpireDate_Object->modify('+1 year');
			
			//formatting the dates
			$EnrollMonth =	date_format($EnrollDate_Object, 'F');
			$EnrollYear	 =	date_format($EnrollDate_Object, 'Y');
			$ExpireYear	 =	date_format($ExpireDate_Object, 'Y');
			}
		catch(Exception $e)
			{
			echo $e->getMessage();
			exit(1);
			}
		
		//Correctly formats the meter number dte meter numbers and consumers pod numbers are treated differently
		$MeterNumber = $this->input->post('MeterNumber');
		//prep data based on which utility
		if($contractSearch['Utility'] == "Consumers" && $MeterNumber != '')
			{
			while(strlen($MeterNumber) < 13)
				{$MeterNumber = '0'.$MeterNumber;}
			}
		
		//format phone and fax numbers as either 1(555)555-5555, (555)555-5555, or 555-5555 
		//depending on the string length after being stripped of non numerical characters
		$Phone = teleformat($this->input->post("Phone"));
		$Fax = teleformat($this->input->post("Fax"));
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
		//Assign the correct .css file to the corresponding docType
		if($this->input->post('DocType') == "Contract"){$CSSfile = 'style-pdf-contract.css';}
		else{$CSSfile = 'style-pdf-terms.css';}
	
		//If the contract is not populated with information then a secondary css file is required to correct for certian formatting issues
		//The variable is given with the full path so that it will not leave remaining 
		if($this->input->post('populatedState') == '0'){$CSSfilesecondary = '<LINK REL=StyleSheet HREF="'.FCPATH.'css/unpopulated.css" TYPE="text/css" MEDIA=screen>';}
		else{$CSSfilesecondary = '';}
	
		if($this->input->post('MailCity') != '')
			{$mailAddressLine2 = $this->input->post('MailCity').', '.$this->input->post('MailState').' '.$this->input->post('MailZip');}
		else {$mailAddressLine2 = '';}
		
		//The $data array is assembled then send to the sample view for use
		$data = array(
			'CSSfile'			=>	$CSSfile,
			'CSSfilesecondary'	=>	$CSSfilesecondary,
			'contractVerbage'	=>	$this->contract_model->contract_verbage($contractSearch),
			'SrvName'			=>	$this->input->post('SrvName'),
			'AccNumber'			=>	$this->input->post('AccNumber'),
			'MeterNumber'		=>	$MeterNumber,
			'Phone'				=>	$Phone,
			'Fax'				=>	$Fax,
			'Email'				=>	$this->input->post('Email'),
			'srvAddressLine1'	=>	$this->input->post('SrvStrNumber').' '.$this->input->post('SrvStrName'),
			'srvAddressLine2'	=>	$this->input->post('SrvCity').', MI  '.$this->input->post('SrvZip'),
			'mailAddressLine1'	=>	$this->input->post('MailStreet'),
			'mailAddressLine2'	=>	$mailAddressLine2,
			'EnrollMonth'		=>	$EnrollMonth,
			'EnrollYear'		=>	$EnrollYear,
			'ExpireYear'		=>	$ExpireYear
			);
	
		//the view is not loaded to the user instead used as the content scaffolding for the DOMPDF object
		$html = $this->load->view('documents/sample',$data,TRUE);		
		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream("sample.pdf", array("Attachment" => 0));
		
		}
	
	function batch()
		{
		
		require_once(FCPATH.'dompdf/dompdf_config.inc.php');
		
				function teleformat($numberString)
					{
					$stringStripped = ereg_replace("[^0-9]", "", $numberString );
					$stringArray = str_split($stringStripped);
					
					if(count($stringArray) == 11)
						{$stringFormatted = $stringArray[0].'('.$stringArray[1].$stringArray[2].$stringArray[3].')'.$stringArray[4].$stringArray[5].$stringArray[6].'-'.$stringArray[7].$stringArray[8].$stringArray[9].$stringArray[10];}
					elseif(count($stringArray) == 10)
						{$stringFormatted = '('.$stringArray[0].$stringArray[1].$stringArray[2].')'.$stringArray[3].$stringArray[4].$stringArray[5].'-'.$stringArray[6].$stringArray[7].$stringArray[8].$stringArray[9];}
					elseif(count($stringArray) == 7)
						{$stringFormatted = $stringArray[0].$stringArray[1].$stringArray[2].'-'.$stringArray[3].$stringArray[4].$stringArray[5].$stringArray[6];}
					else{$stringFormatted = $stringStripped;}
					
					RETURN $stringFormatted;
					}
		
		
		
		//get sessionID and generate sessionID hash
		$sessionID = md5($this->session->userdata('session_id'));
		
		//pull all records that match that hashed sessionID
		$rawDocuments = $this->contract_model->batch($sessionID);
		
		
		switch ($this->uri->segment(3))
			{
			//This case processes online batch jobs which have a couple of different settings than internal batch jobs
			//Generates contract documents verbatim / term documents only generate one copy for each that is of a different type
			case 'online':
				echo $sessionID." - Generate contract documents verbatim / for term documents only generate one copy for each type that is different.<p />";
				print_r($rawDocuments);
				
				
				
				break;
				
			//This case processes internal batch jobs
			//Each document that is requested is generated verbatim no matter the duplicates that may be requested
			case 'internal':
				$html = $this->load->view('documents/doc-header.php',$data,TRUE);
				$dompdf = new DOMPDF();
				
				foreach($rawDocuments as $document)
					{
					$contractSearch = array(
						'Utility'		=>	$document['Utility'],
						'ContractType'	=>	$document['AccType']
						);
					
					//Assign the correct .css file to the corresponding docType
					if($document['DocType'] == "Contract"){$CSSfile = 'style-pdf-contract.css';}
					else{$CSSfile = 'style-pdf-terms.css';}
					
					
					//If the contract is not populated with information then a secondary css file is required to correct for certian formatting issues
					//The variable is given with the full path so that it will not leave remaining 
					if($this->input->post('populatedState') == '0'){$CSSfilesecondary = '<LINK REL=StyleSheet HREF="'.FCPATH.'css/unpopulated.css" TYPE="text/css" MEDIA=screen>';}
					else{$CSSfilesecondary = '';}
					
					if($document['MailCity'] != '')
						{$mailAddressLine2 = $document['MailCity'].', '.$document['MailState'].' '.$document['MailZip'];}
					else {$mailAddressLine2 = '';}
					
					
					//$date is the define enrollment date by the user the contract expiration date is calculated by adding 1 year and changing the month/day to 3-31
					$Date = $document['Date'];
					list($year, $month, $day) = split('[-]', $Date);
		
					//Creates date objects and correctly formats them for use in the pdf text
					try{
						//EnrollDate remains the same as the selected date by the user
						$EnrollDate_Object = new DateTime($Date);
						$ExpireDate_Object = new DateTime($year.'-03-31');
						$ExpireDate_Object->modify('+1 year');
			
						//formatting the dates
						$EnrollMonth =	date_format($EnrollDate_Object, 'F');
						$EnrollYear	 =	date_format($EnrollDate_Object, 'Y');
						$ExpireYear	 =	date_format($ExpireDate_Object, 'Y');
						}
						
					catch(Exception $e)
						{
						echo $e->getMessage();
						exit(1);
						}
					
					$data = array(
						'CSSfile'			=>	$CSSfile,
						'CSSfilesecondary'	=>	$CSSfilesecondary,
						'contractVerbage'	=>	$this->contract_model->contract_verbage($contractSearch),
						'SrvName'			=>	$document['SrvName'],	//$this->input->post('SrvName'),
						'AccNumber'			=>	$document['SrvName'],	//$this->input->post('AccNumber'),
						'MeterNumber'		=>	$document['MeterNumber'],
						'Phone'				=>	$document['Phone'],
						'Fax'				=>	$document['Fax'],
						'Email'				=>	$document['SrvName'],	//$this->input->post('Email'),
						'srvAddressLine1'	=>	$document['SrvName'],	//$this->input->post('SrvStrNumber').' '.$this->input->post('SrvStrName'),
						'srvAddressLine2'	=>	$document['SrvName'],	//$this->input->post('SrvCity').', MI  '.$this->input->post('SrvZip'),
						'mailAddressLine1'	=>	$document['SrvName'],	//$this->input->post('MailStreet'),
						'mailAddressLine2'	=>	$mailAddressLine2,
						'EnrollMonth'		=>	$EnrollMonth,
						'EnrollYear'		=>	$EnrollYear,
						'ExpireYear'		=>	$ExpireYear,
						'EndLine'			=>	''
						);
					
					//the view is not loaded to the user instead used as the content scaffolding for the DOMPDF object
					$html = $this->load->view('documents/sample',$data,TRUE);
					

					}
					
				$html = $html.' '.$this->load->view('documents/doc-footer.php',$data,TRUE);
					
				$dompdf->load_html($html);
				$dompdf->render();
				$dompdf->stream("sample.pdf", array("Attachment" => 0));
			
					
				break;
			
			//If a case match is not met then the URI segment was typed incorrectly
			//At some point this will be changed to a 404 page
			default:
				redirect('contracts','refresh');
				break;
			}
		}
	}