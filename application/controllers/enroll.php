<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enroll extends CI_Controller 
	{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
		{
		date_default_timezone_set('America/Detroit');
		$rep = $this->enroll_model->get_Rep($this->uri->segment(2));
		
		$data = array(
			'title'		=>	'Bishop Energy Online Enrollment',
			'rep'		=>	$rep,
			'script'	=>	'validate.js'
				);
		
		$this->form_validation->set_rules('Utility', 'Natural Gas Utility', 'required|trim');
		$this->form_validation->set_rules('Cust_Type', 'Service Type', 'required|trim');
		
		//Set Rules for Consumers
		if($this->input->post('Utility') == 'consumers')
			{
			//Account Information
			$this->form_validation->set_rules('con_service_name', 'Service Name', 'required|trim');
			$this->form_validation->set_rules('Consumers_Acc_Number', 'Consumers Energy Account Number', 'required|trim');
			$this->form_validation->set_rules('POD_Number', 'Natural Gas POD Number', 'required|trim');
			//Address Information
			$this->form_validation->set_rules('con_service_street', 'Service Street Address', 'required|trim');
			$this->form_validation->set_rules('con_service_city', 'Service City', 'required|trim');
			$this->form_validation->set_rules('con_service_zip', 'Service Zip Code', 'required|trim');
			}
		
		//set Rules for DTE
		else
			{
			//Account Information
			$this->form_validation->set_rules('dte_service_name', 'Service Name', 'required|trim');
			$this->form_validation->set_rules('DTE_Acc_Number', 'Detroit Edison Account Number', 'required|trim');
			$this->form_validation->set_rules('Meter_Number', 'Natural Gas Meter Number', 'required|trim');
			//Address Information
			$this->form_validation->set_rules('Serv_Street_Number', 'Service Address House Number', 'required|trim');
			$this->form_validation->set_rules('dte_service_street', 'Service Address Street Name', 'required|trim');
			$this->form_validation->set_rules('dte_service_city', 'Service Address City', 'required|trim');
			$this->form_validation->set_rules('dte_service_zip', 'Service Address Zip Code', 'required|trim');
			}
		
		//Standard Set of Rules
		$this->form_validation->set_rules('Comp_Name', 'Company Name', 'trim');
		
		$this->form_validation->set_rules('Cust_F_Name', 'First Name', 'required|trim');
		$this->form_validation->set_rules('Cust_MI', 'Middle Initial', 'trim');
		$this->form_validation->set_rules('Cust_L_Name', 'Last Name', 'required|trim');
		$this->form_validation->set_rules('Phone1', 'Phone Number', 'required|trim');
		$this->form_validation->set_rules('Comments', 'Comments', 'trim');
		
		//Email address is not required when a rep is siging an account up
		if(!$rep[0]['CharCode'])
			{$this->form_validation->set_rules('Email', 'Email Address', 'required|trim');}
			
		//Service / mailing address Rule Sets
		if(!$this->input->post('service_mail'))
			{
			$this->form_validation->set_rules('Mail_Address', 'Mailing Street Address', 'required|trim');
			$this->form_validation->set_rules('Mail_City', 'Mailing Address City', 'required|trim');
			$this->form_validation->set_rules('Mail_State', 'Mailing Address State', 'required|trim');
			$this->form_validation->set_rules('Mail_Zip', 'Mailing Zip Code', 'required|trim');
			}
		else
			{
			$this->form_validation->set_rules('Mail_Address', 'Mailing Street Address', 'trim');
			$this->form_validation->set_rules('Mail_City', 'Mailing Address City', 'trim');
			$this->form_validation->set_rules('Mail_State', 'Mailing Address State', 'trim');
			$this->form_validation->set_rules('Mail_Zip', 'Mailing Zip Code', 'trim');
			}
		//if Validation hasn't been run or run and failed
		if ($this->form_validation->run() === FALSE)
			{
			$this->load->view('template/header', $data);
			$this->load->view('enroll-view', $data);
			$this->load->view('template/footer');
			}
		//if validation has been run and passed
		else
			{
			//progInfo has most of the date and price information for customer choice program
			$progInfo = $this->enroll_model->prog_info();
			
			//baseInfo is a combination of default info and utility dependent variables
			//blank entries are created where values will be added later
			$baseInfo = array(
				'Enroll_Source'			=>	'Online',
				'Cur_Status'			=>	'unconfirmed',
				'Submit_date'			=>	date('Y-m-d H:i:s'),
				'Enroll_Date'			=>	'',
				'Contract_Expiration'	=>	'',
				'Price_Cat'				=>	'',
				'Serv_Name'				=>	'',
				'Serv_Address'			=>	'',
				'Serv_City'				=>	'',
				'Serv_State'			=>	'MI',
				'Serv_Zip'				=>	'',
				'Mail_Address'			=>	'', 
				'Mail_City'				=>	'',
				'Mail_State'			=>	'',
				'Mail_Zip'				=>	'',
				'Log'					=>	'Entry created online: '.date('Y-m-d H:i:s')
				);
			
			// based on the utility, form entries need to be set and unset due to the input having both consumer and dte entries
			if($this->input->post('Utility') == "consumers")
				{
				$baseInfo['Enroll_Date'] 		 = $progInfo[0]['Con_Enroll_Month'];
				$baseInfo['Contract_Expiration'] = $progInfo[0]['Con_Prog_End'];
				$baseInfo['Price_Cat'] 			 = $progInfo[0]['Con_Default_Cat'];
				$baseInfo['Serv_Name']			 = $this->input->post('con_service_name');
				$baseInfo['Serv_Address']		 = $this->input->post('con_service_street');
				$baseInfo['Serv_City']			 = $this->input->post('con_service_city');
				$baseInfo['Serv_Zip']			 = $this->input->post('con_service_zip');
				
				$combined = array_merge((array)$baseInfo, (array)$this->input->post());
				
				unset($combined['DTE_Acc_Number'],$combined['Meter_Number'],$combined['Serv_Street_Number']);
				
				//Set session variables
				$session = array(
				'utility'		=>	$combined['Utility'],
				'accountType'	=>	$this->input->post('Cust_Type')
					);
				
				//detect mail service thing and set mail variables
				if($this->input->post('service_mail'))
					{
					//set all service address information as mail address entries
					$baseInfo['Mail_Address'] 	= $baseInfo['Serv_Address'];
					$baseInfo['Mail_City']		= $baseInfo['Serv_City'];
					$baseInfo['Mail_State']		= 'MI';
					$baseInfo['Mail_Zip']		= $baseInfo['Serv_Zip'];
					}
				}
				
			if($this->input->post('Utility') == "dte")
				{
				/*
				try{ //correctly format the meterRead date from MM/DD/YYYY to YYYY-MM-DD
					$meterRead = $this->input->post('Meter_Read');
					list($month, $day, $year) = split('/', $meterRead);
					$meterRead = new DateTime($year.'-'.$month.'-'.$day);
					$Meter_Read = date_format($meterRead, 'Y-m-d');
					}
				catch(Exception $e)
					{
					echo $e->getMessage();
					exit(1);
					}				
				*/
				
				$baseInfo['Enroll_Date'] 		 	= $progInfo[0]['DTE_Enroll_Month'];
				$baseInfo['Contract_Expiration'] 	= $progInfo[0]['DTE_Prog_End'];
				$baseInfo['Price_Cat'] 			 	= $progInfo[0]['DTE_Default_Cat'];
				$baseInfo['Serv_Name']			 	= $this->input->post('dte_service_name');
				$baseInfo['Serv_Address']		 	= $this->input->post('dte_service_street');
				$baseInfo['Serv_City']			 	= $this->input->post('dte_service_city');
				$baseInfo['Serv_Zip']				= $this->input->post('dte_service_zip');
				
				$combined = array_merge((array)$baseInfo, (array)$this->input->post());
				
				//$combined['Meter_Read'] = $Meter_Read;
				
				unset($combined['POD_Number'],$combined['Consumers_Acc_Number']);
				
				//Set session variables
				$session = array(
				'utility'		=>	$combined['Utility'],
				'accountType'	=>	$this->input->post('Cust_Type')
					);
				
				//detect mail service thing and set mail variables
				if($this->input->post('service_mail') == "TRUE")
					{
					//set all service address information as mail address entries
					$baseInfo['Mail_Address']	= $this->input->post('Serv_Street_Number').' '.$baseInfo['Serv_Address'];
					$baseInfo['Mail_City']		= $baseInfo['Serv_City'];
					$baseInfo['Mail_State']		= 'MI';
					$baseInfo['Mail_Zip']		= $baseInfo['Serv_Zip'];
					}
				}
			
			unset($combined['repID'],$combined['repName'],$combined['con_service_name'],$combined['service_mail'],$combined['dte_service_name'],$combined['con_service_street'],$combined['con_service_city'],$combined['con_service_zip'],$combined['dte_service_street'],$combined['dte_service_city'],$combined['dte_service_zip']);
			
			//Send info to database
			//$insertResults = $this->enroll_model->insert_record('CE_Enrollments', $combined);
			$insertResults = 1;
			//Add utility and account type info to user session variables
			$this->session->set_userdata($session);
			$this->session->set_userdata('email', $_POST['Email']);
			
			if($insertResults < 1)
				{
				$this->form_error();
				}
			else
				{
				//form submission successfule load form_success() function
				$this->form_success();
				}
			}
		}
		
	function form_success()
		{
		//cURL request should be made to http://bishopenergy.com/online/api/proginfo_get/$this->session->userdata('utility')/$this->session->userdata('accountType')
		//Set the URL to to execute the CURL request
		$url = 'http://bishopenergy.com/online/api/proginfo_get/'.$this->session->userdata('utility').'/'.$this->session->userdata('accountType');
		
		//Set the CURL options
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER	=>	1,
			CURLOPT_URL				=>	$url
			));
		
		//Execute the CURL request
		$result = curl_exec($curl);
		
		//Close the CURL object
		curl_close($curl);
		
		//decode the json object into an array
		$resultArray = json_decode($result, true);
		
		$data = array(
			'title'				=>	'Submission successful! - Bishop Energy',
			'script'			=>	'',
			'programTerms'		=>	$resultArray[0]['ProgramTerms'],
			'acknowledgement'	=>	$resultArray[0]['Acknowledgement'],
			'confirmURL'		=>	'http://bishopenergy.com/online/confirm/?email='.urlencode($this->session->userdata('email')).'&key='.md5($this->session->userdata('session_id'))
			);
		
		//print_r($this->input->post());
		
		$dataInput = array
			(
			'Enroll_Source'	=>	'online',
			'Cur_Status'	=>	'created',
			'Submit_date'	=>	date('Y-m-d'),
			'Sales_Rep'		=>	$this->input->post('Sales_Rep'),
			'Utility'		=>	$this->input->post('Utility'),
			'Cust_Type'		=>	$this->input->post('Cust_Type'),
			'REG_ID'		=>	md5($this->session->userdata('session_id')),
			'Sales_Rep'		=>	$this->input->post('Sales_Rep'),
			'Comp_Name'		=>	$this->input->post('Comp_Name'),
			'Cust_F_Name'	=>	$this->input->post('Cust_F_Name'),
			'Cust_MI'		=>	$this->input->post('Cust_MI'),
			'Cust_L_Name'	=>	$this->input->post('Cust_L_Name'),
			'Phone1'		=>	$this->input->post('Phone1'),
			'Email'			=>	$this->input->post('Email'),
			'Comments'		=>	$this->input->post('Comments')
			);
		
		if($this->session->userdata('utility') === 'consumers')
			{
			//retrieve default price category based on utility
			
			//$dataInput['Price_Cat'] 			= ;
			//$dataInput['Contract_Expiration'] 	= ;
			$dataInput['Log']					= 'Entry created online: '.date('Y-m-d H:i:s');
			$dataInput['Serv_Name']				= $this->input->post('con_service_name');
			$dataInput['Consumers_Acc_Number']	= $this->input->post('Consumers_Acc_Number');
			$dataInput['POD_Number']			= $this->input->post('POD_Number');
			$dataInput['Serv_Address']			= $this->input->post('con_service_street');
			$dataInput['Serv_State']			= 'MI';
			$dataInput['Serv_City']				= $this->input->post('con_service_city');
			$dataInput['Serv_Zip']				= $this->input->post('con_service_zip');
			
			if(isset($_POST['service_mail']))
				{
				$dataInput['Mail_Address'] 	= $this->input->post('con_service_street');
				$dataInput['Mail_City'] 	= $this->input->post('con_service_city');
				$dataInput['Mail_State']	= 'MI';
				$dataInput['Mail_Zip'] 		= $this->input->post('con_service_zip');
				}
			else
				{
				$dataInput['Mail_Address'] 	= $this->input->post('Mail_Address');
				$dataInput['Mail_City'] 	= $this->input->post('Mail_City');
				$dataInput['Mail_State']	= $this->input->post('Mail_State');
				$dataInput['Mail_Zip'] 		= $this->input->post('Mail_Zip');
				}
			}
		
		else if ($this->session->userdata('utility') === 'dte')
			{
			$dataInput['Serv_Name']				= $this->input->post('dte_service_name');
			$dataInput['DTE_Acc_Number']		= $this->input->post('DTE_Acc_Number');
			$dataInput['Meter_Number']			= $this->input->post('Meter_Number');
			$dataInput['Meter_Read']			= $this->input->post('Meter_Read');
			$dataInput['Serv_Street_Number']	= $this->input->post('Serv_Street_Number');
			$dataInput['Serv_Address']			= $this->input->post('dte_service_street');
			$dataInput['Serv_State']			= 'MI';
			$dataInput['Serv_City']				= $this->input->post('dte_service_city');
			$dataInput['Serv_Zip']				= $this->input->post('dte_service_zip');
			
			if(isset($_POST['service_mail']))
				{
				$dataInput['Mail_Address'] 	= $this->input->post('Serv_Street_Number').' '.$this->input->post('dte_service_street');
				$dataInput['Mail_City'] 	= $this->input->post('dte_service_city');
				$dataInput['Mail_State']	= 'MI';
				$dataInput['Mail_Zip'] 		= $this->input->post('dte_service_zip');
				}
			else
				{
				$dataInput['Mail_Address'] 	= $this->input->post('Mail_Address');
				$dataInput['Mail_City'] 	= $this->input->post('Mail_City');
				$dataInput['Mail_State']	= $this->input->post('Mail_State');
				$dataInput['Mail_Zip'] 		= $this->input->post('Mail_Zip');
				}
			}
		else{echo 'The utility was not found.';}
		
		//send $dataInput to the Model
		//if affected rows == 1 then all is well
		$result = $this->enroll_model->insert_record('CE_Enrollments', $dataInput);
		
		if($result > 0) //as long as at least 1 row was affected (should only be 1)
			{
				
				$message = $this->load->view('emails/confirm-view.php','',TRUE);
				
				//send email to confirm everything
				$this->email->from('mkelly@bishopenergy.com', 'Mike Kelly');
				$this->email->to('kellysystemsrepair@gmail.com'); 
				
				$this->email->subject('Email Test');
				$this->email->message($message);	
				
				$this->email->send();
				
				
				//echo $this->email->print_debugger();
				
				
				
				//echo phpinfo();
				
				//$this->load->view('emails/confirm-view');
				
				//$this->load->view('template/header', $data);
				//$this->load->view('formSuccess-view', $data);
				//$this->load->view('template/footer');
			}
		else{echo 'There was a problem inserting the record into the database';}
		
		}
	
	function form_error()
		{
		echo 'Indescribable horrors have occured!  Oh the humanity!';
		}
	}

/* End of file enroll.php */
/* Location: ./application/controllers/enroll.php */