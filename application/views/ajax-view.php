				<legend><h5>(<?php echo $count;?>) Batch Jobs</h5></legend>
				<div>
					<table id="ajaxTable" class="table table-hover">
						<thead>
							<tr>
								<th>Utility</th>
								<th>Acc Type</th>
								<th><center>Document Type</center></th>
								<th>Acc Number</th>
								<th>Service Name</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($records as $values){ ?>
							<tr>
								<td><?php echo $values['Utility'];?></td>
								<td><?php echo $values['AccType'];?></td>
								<td><center><?php echo $values['DocType'];?></center></td>
								<td><?php echo $values['AccNumber'];?></td>
								<td><?php echo $values['SrvName'];?></td>
							</tr>
							<?php }	unset($values);?>
						</tbody>
					</table>
					<button id="batchButton" type="button" onclick="window.location='<?php echo base_url('contracts/batch/internal');?>';" class="btn btn-success btn-small pull-right" tabindex=25>Process Batch</button>
				</div>
				