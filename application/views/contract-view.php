<body>
	<div class="container-narrow" style="align: center;">
		
		<div class="row">
			<div class="span10">
			
				<!-- CONTENT HERE -->
				<h1>Contracts</h1>
				<div class="alert alert-error" style="display: none;">Contract type information is required</div>
				<form method="post" accept-charset="utf-8" action="<?php echo site_url("contracts/pdf");?>" />
					<table class="table">
						<tr>
							<td colspan="2" >
								
								<span class="input-prepend control-group error">
									<span class="add-on">Utility</span>
									<select class="span2" id="Utility" name="Utility" tabindex=1>
										<option value="0"></option>
										<option value="Consumers">Consumers</option>
										<option value="DTE">DTE</option>
									</select>
								</span>
								&nbsp;&nbsp;&nbsp;
								<span class="input-prepend control-group error">
									<span class="add-on">Type</span>
									<select class="span3" id="AccType" name="AccType" tabindex=2>
										<option value="0"></option>
										<option value="Residential">Residential</option>
										<option value="Small Commercial">Small Commercial</option>
										<option value="Large Commercial">Large Commercial</option>
									</select>
									<input type="hidden" name="sessionID" id="sessionID" value="<?php echo $sessionID;?>">
									<input type="hidden" name="populatedState" id="populatedState" value="0">
								</span>
								&nbsp;&nbsp;&nbsp;
								<span class="input-prepend control-group error">
									<span class="add-on">Date</span>
									
									<select class="span2" id="Date" name="Date" tabindex=3>
											<option></option>
									</select>
									<select class="span2" id="consumersDate" name="Date" style="display: none;" disabled="true" tabindex=3>
											<?php foreach($ConsumersdateOptions as $option){echo $option;} ?>
									</select>
									<select class="span2" id="dteDate" name="Date" style="display: none;" disabled="true" tabindex=3>
											<?php foreach($DTEdateOptions as $option){echo $option;} ?>
									</select>
								</span>
								<br />
								<button type="button" class="btn btn-link" id="addinfo" tabindex=5><i class="icon-plus"></i> Add Customer Info</button>
							</td>
							<td>
								<span class="control-group error">
									<label class="radio error">
										<input type="radio" id="DocType" name="DocType" value="Contract" tabindex=3>
										Contract
									</label>
									<label class="radio">
										<input type="radio" id="DocType" name="DocType" value="Terms" tabindex=4>
										Terms
									</label>
								</span>
							</td>
						</tr>
					</table>
				<!-- <hr class="compressedHR"/> -->
				<div class="row">
					<div id="custInfo" style="display: none;">
						<div class="span4 offset1">
							<fieldset>
								<h5>Customer Information</h5>
								<input id="FirstName" name="FirstName" class="input-large" type="text" placeholder="First Name" tabindex=6>
								<input id="LastName" name="LastName" class="input-large" type="text" placeholder="Last Name" tabindex=7>
								<h5>Account Information</h5>
								<input id="AccNumber" name="AccNumber" class="input-large" type="text" placeholder="Account Number" tabindex=10>
								<input id="MeterNumber" name="MeterNumber" class="input-large" type="text" placeholder="Meter Number"tabindex=11>
								<h5>Contact Information</h5>
								<input id="Email" name="Email" class="input-xlarge" type="text" placeholder="Email Address" tabindex=16>
								<input id="Phone" name="Phone" class="input-medium" type="text" placeholder="Phone Number" tabindex=17>
								<input id="Fax" name="Fax" class="input-medium" type="text" placeholder="Fax Number" tabindex=18>
							</fieldset>
							<br />
						</div>
						<div class="span4">
							<fieldset>
								<h5>&nbsp;</h5>
								<input id="SrvName" name="SrvName" class="input-large" type="text" placeholder="Service Name" tabindex=8>
								<input id="CompName" name="CompName" class="input-large" type="text" placeholder="Company Name" tabindex=9>
								<h5>Service Address</h5>
								<input id="SrvStrNumber" name="SrvStrNumber" class="input-mini" type="text" placeholder="Street #" tabindex=12> <input id="SrvStrName" name="SrvStrName" class="input" type="text" placeholder="Street Name" tabindex=13>
								<input id="SrvCity" name="SrvCity" class="input-small" type="text" placeholder="City" tabindex=14> <input id="SrvState" name="SrvState" class="input-mini" type="text" value="MI" disabled> <input id="SrvZip" name="SrvZip" class="input-small" type="text" placeholder="Zip" tabindex=15>
								<h5>Mailing Address</h5>
								<input id="MailStreet" name="MailStreet" class="input-xlarge" type="text" placeholder="Street" tabindex=19>
								<input id="MailCity" name="MailCity" class="input-small" type="text" placeholder="City" tabindex=20> <input id="MailState" name="MailState" class="input-mini" type="text" placeholder="State" tabindex=21> <input id="MailZip" name="MailZip" class="input-small" type="text" placeholder="Zip" tabindex=22>
								<input id="sessionID" name="sessionID" type="hidden" value="<?php echo $sessionID; ?>">
							</fieldset>
						</div>
						<div class="span8">
						</div>
					</div> <!-- endDiv - custInfo -->
				</div> <!-- endDiv - row -->
 				
				<button type="submit" class="btn btn-primary btn-small pull-right" tabindex=24>Process</button>
				<button id="addButton" type="button" class="btn btn-success btn-small pull-right" tabindex=23><i class="icon-plus icon-white"></i></button> 
				
				</form>
			</div>
		</div><!-- end div row-->
		<div class="row">
			<div class="span10" id="batchReport" style="display: none;">
				<!-- Ajax results show here of how many documents are in the batch job-->
			</div>
		</div>
		<div class="footer">
			<p>&copy; Company <?php echo date('Y');?></p>
		</div>
	
	</div> <!-- /container -->