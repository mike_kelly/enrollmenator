
				<script type="text/php">
					if ( isset($pdf) ) 
						{
						$font = Font_Metrics::get_font("times", "normal");
						$pdf->page_text(170, 735, "1152 James Savage Road '."$character".' Midland, Michigan 48640", $font, 12, array(0,0,0));
						$pdf->page_text(245, 750, "www.bishopenergy.com", $font, 12, array(0,0,0));
						}
				</script>
					<div id="header"><img src="<?php echo FCPATH ?>img/contracts/header.png" width="718" height="59"></img></div>
					<div id="sidetext"><img src="<?php echo FCPATH ?>img/contracts/sidetext.png"></img></div>
					
					<div id="title"><strong><?php echo $contractVerbage[0]['ContractType'];?><br />Variable Rate / Natural Gas Customer Choice Contract Between Bishop Energy Services, LLC (Supplier) and</strong></div>
					<div id="srvName"><?php echo $SrvName; ?></div>
					<div class="maintitle" id="buyer">Buyer:</div><hr id="buyerhr"/>
					<div id="buyerexplain">(Buyer is Account Holder or Legally Authorized Person)</div>
					
					<div id="terms">
						<p>
						Supplier agrees to sell Buyer natural gas for an initial term beginning with your <?php echo $EnrollMonth.', '.$EnrollYear; ?> billing cycle through March 31st, <?php echo $ExpireYear.'. '.$contractVerbage[0]['ContractTerms'];?>
						</p>
						<p id="secondP">
						The price of natural gas for the initial contract period will be determined monthly.  This <strong>variable rate</strong> will be a monthly cumulative weighted average cost of gas (WACOG) 
						consisting of a supply portfolio of fixed and cash purchases plus all administration fees.  Example, Month (1) = 5,000 Mcf @ $4.95 ($24,750); Month (2) = 5,900 Mcf @ $5.35 ($31,565).  
						The WACOG = $5.167 ((24,570+31,565)/10,900).
						</p>
					</div>
					
					<div id="addresses">
						<div id="serviceHeader" class="formfields"><strong>Service Address (on utility bill)</strong></div> <div id="mailingHeader" class="formfields"><strong>Mailing Address (if different)</strong></div>
						<hr id="serviceFirst"/> <hr id="mailingFirst"/>
						<hr id="serviceSecond"/> <hr id="mailingSecond"/>
					</div>
					
					<div class="entryFont" id="AccNumber"><?php echo $AccNumber; ?></div>
					<div class="entryFont" id="MeterNumber"><?php echo $MeterNumber; ?></div>
					<!-- <div class="entryFont" id="Phone"><?php echo $Phone; ?></div> -->
					
					<div id="accInfo">
						<span id="accNumber" class="formfields">Consumers Energy Acct#:<div class="hr"></div> (please attach copy of utility bill)</span><br />
						<span id="podNumber" class="formfields">Consumers Energy POD#:<div class="hr"></div> Est. Annual Usage:<div class="hr">&nbsp;</div><div id="mcf">Mcf</div></span>
					</div>
					
					<div class="entryFont" id="srvAddressLine1"><?php echo $srvAddressLine1; ?></div>
					<div class="entryFont" id="srvAddressLine2"><?php echo $srvAddressLine2; ?></div>
					<div class="entryFont" id="mailAddressLine1"><?php echo $mailAddressLine1; ?></div>
					<div class="entryFont" id="mailAddressLine2"><?php echo $mailAddressLine2; ?></div>
					
					<div class="entryFont" id="Phone"><?php echo $Phone; ?></div>
					<div class="entryFont" id="Fax"><?php echo $Fax; ?></div>
					<div class="entryFont" id="Email"><?php echo $Email; ?></div>
					<div id="contactInfo">
						<span class="formfields">Phone:</span> <div class="hr"></div> <span class="formfields">Fax:</span> <div class="hr"></div> <div id="emailLabel" class="formfields">Email:</div> <div class="hr"></div>
					</div>
					
					<hr id="termsHR" style=""/>
					
					<div id="bottomterms">
						<p><span id="contracknowledge"><?php echo $contractVerbage[0]['ContractAcknowledge'];?>
						If I return to the utility, a $10 fee may apply and I must remain with them for 12 months. 
						A Confirmation Letter will be mailed to you within 7 days to confirm your enrollment.</strong></p>
					</div>
					
					<div id="seller">Seller - BishopEnergy Services, LLC</div>
					<div id="sellerDate">Date:<div class="hr"></div></div>
					<div id="sellerHR" class="hr"></div>
					<div id="sellerSigTitle">Supplier's Authorized Signature</div>
					
					<div id="buyerDate">Date:<div class="hr"></div></div>
					<div id="buyerPrint">Print:<div class="hr"></div></div>
					<div id="buyerHR" class="hr"></div>
					<div id="buyerSigTitle">Account Holder or Legally Authorized Signature</div>
					<div id="buyerID">If legally authorized, what is your relationship to <br />Account Holder?<div class="hr"></div></div>
					
					<!-- <div id="officeUse">For Internal Use - Category:[<div class="hr"></div><div id="officeUse1">] Init:[</div><div class="hr"></div><div id="officeUse2">] File Name:</div><div class="hr"></div></div> -->
					<!-- <div style="page-break-after:always"></div> -->
					<?php echo $EndLine; ?>
