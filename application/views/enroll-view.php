<body>
	<div class="container-narrow" style="align: center;">
		
		<div class="row">
			<div id="value"></div>
			<div class="span8">
				<?php 
					if(isset($rep))
						{echo '<div class="alert alert-success"><i class="icon-thumbs-up"></i> <strong>Hello '.$rep[0]['Name'].'<span class="pull-right"> Rep Code: '.$rep[0]['CharCode'].'</span></strong></div>';}
				?>
				<h2>Bishop Energy Online Enrollment</h2>
				<hr />
			</div>
			<div class="span8">
				<form method="post" accept-charset="utf-8" action="<?php echo current_url();?>" />
				<div class="control-group" id="utility_group"> <!-- completed -->
					<label class="description" for="Utility">Natural Gas Utility</label>
					<select id="Utility" class="maindrops" name="Utility">
						<option value="" 			<?php if($this->input->post('Utility')== ""){echo "selected";}?>>...</option>
						<option value="consumers" 	<?php if($this->input->post('Utility')== "consumers"){echo "selected";}?>>Consumers Energy</option>
						<option value="dte" 		<?php if($this->input->post('Utility')== "dte"){echo "selected";}?>>Detroit Energy</option>
					</select>
					<span class="help-inline lead" id="utility_error" style="display: none;"><em>Select your utility</em></span>
				</div>
				
				<div class="control-group" id="service_group"> <!-- completed -->
					<label class="description" for="Cust_Type">Service Type</label>
					<a class="btn btn-small pull-right consumers" href="#con_service_type_modal" role="button" data-toggle="modal" title="Service Type"><i class="icon-question-sign"></i></a>
					<a class="btn btn-small pull-right dte" href="#dte_service_type_modal" role="button" data-toggle="modal" title="Service Type"><i class="icon-question-sign"></i></a>
					<a class="btn btn-small pull-right dummy" href="#service_type_modal" role="button" data-toggle="modal" title="Service Type"><i class="icon-question-sign"></i></a>
					<select id="Cust_Type" class="maindrops" name="Cust_Type">
						<option value="" <?php if($this->input->post('Cust_Type') == ""){echo "selected";}?>>...</option>
						<option value="residential" <?php if($this->input->post('Cust_Type') == "residential"){echo "selected";}?>>Residential</option>
						<option value="small" <?php if($this->input->post('Cust_Type') == "small"){echo "selected";}?>>Small Commercial</option>
						<option value="large" <?php if($this->input->post('Cust_Type') == "large"){echo "selected";}?>>Large Commercial</option>
					</select>
					<span class="help-inline lead" id="service_error" style="display: none;"><em>Select a Service Type</em></span>
				</div>
				<legend>Program Terms</legend>
				<div>
					<p id="terms">
					</p>
				</div>
				<div class="alert alert-block alert-error" id="val_errors" style="display: none;">
					<?php echo validation_errors(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span6">
				<div id="contact_form">
					
					<input type="hidden" id="repID" name="repID" value="<?php if(isset($rep[0]['ID'])){echo $rep[0]['ID'];}else{echo set_value('repID');}?>">
					<input type="hidden" id="repName" name="repName" value="<?php if(isset($rep[0]['Name'])){echo $rep[0]['Name'];}else{echo set_value('repName');}?>">
					<input type="hidden" id="Sales_Rep" name="Sales_Rep" value="<?php if(isset($rep[0]['CharCode'])){echo $rep[0]['CharCode'];}else{;echo set_value('Sales_Rep');}?>">
					
					<fieldset>
						<legend>Account Information</legend>
						
						<div class="consumers"> <!-- completed -->
							<div class="control-group" id="con_service_name_group">
							<label class="description" for="con_service_name">Service Name</label>
							<input type="text"  value="<?php echo set_value('con_service_name'); ?>" name="con_service_name" id="con_service_name" class="input-xlarge" rel="popover"> <a class="btn btn-small pull-right" href="#con_serv_name_modal" role="button" data-toggle="modal" title="Service Name"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="con_service_name_error" style="display: none;"><em>The Service Name field is required</em></span>
							</div>
						</div>
						
						<div class="dte"> <!-- completed -->
							<div class="control-group" id="dte_service_name_group">
							<label class="description" for="dte_service_name">Service Name</label>
							<input type="text"  value="<?php echo set_value('dte_service_name'); ?>" name="dte_service_name" id="dte_service_name" class="input-xlarge" rel="popover"> <a class="btn btn-small pull-right" href="#dte_serv_name_modal" role="button" data-toggle="modal" title="Service Name"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="dte_service_name_error" style="display: none;"><em>The Service Name field is required</em></span>
							</div>
						</div>
						
						<div class="consumers"> <!-- completed -->
							<div class="control-group" id="con_account_number_group">
							<label class="description" for="Consumers_Acc_Number">Account Number</label>
							<input type="text" name="Consumers_Acc_Number" value="<?php echo set_value('Consumers_Acc_Number'); ?>" id="Consumers_Acc_Number" rel="popover"><a class="btn btn-small pull-right" href="#con_account_number_modal" role="button" data-toggle="modal" title="Account Number"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="con_account_number_error" style="display: none;"><em>The Account Number field is required</em></span>
							</div>
						</div>
						
						<div class="dte"> <!-- completed -->
							<div class="control-group" id="dte_account_number_group">
							<label class="description" for="DTE_Acc_Number">Account Number</label>
							<input type="text" name="DTE_Acc_Number" value="<?php echo set_value('DTE_Acc_Number'); ?>" id="DTE_Acc_Number" rel="popover"><a class="btn btn-small pull-right" href="#dte_account_number_modal" role="button" data-toggle="modal" title="Account Number"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="dte_account_number_error" style="display: none;"><em>The Account Number field is required</em></span>
							</div>
						</div>
						
						<div class="consumers"> <!-- completed -->
							<div class="control-group" id="con_meter_group">
							<label class="description" for="POD_Number">Natural Gas POD Number</label>
							<input type="text" name="POD_Number" value="<?php echo set_value('POD_Number'); ?>" id="POD_Number" rel="popover"><a class="btn btn-small pull-right" href="#con_meter_modal" role="button" data-toggle="modal"title="Natural Gas Meter Number"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="con_meter_error" style="display: none;"><em>The Account Number field is required</em></span>
							</div>
						</div>
						
						<div class="dte"> <!-- completed -->
							<div class="control-group" id="dte_meter_group">
							<label class="description" for="Meter_Number">Natural Gas Meter Number</label>
							<input type="text" name="Meter_Number" value="<?php echo set_value('Meter_Number'); ?>" id="Meter_Number" rel="popover"><a class="btn btn-small pull-right" href="#dte_meter_modal" role="button" data-toggle="modal"title="Natural Gas Meter Number"><i class="icon-question-sign"></i></a>
							<span class="help-inline lead" id="dte_meter_error" style="display: none;"><em>The Account Number field is required</em></span>
							</div>
						</div>
						
						<div class="dte"> <!-- completed -->
							<div class="control-group" id="dte_meterread_group">
								<label class="description" for="Meter_Read">Next Meter Read Date</label>
								<input type="text" name="Meter_Read" value="<?php echo set_value('Meter_Read'); ?>" id="Meter_Read" rel="popover"><a class="btn btn-small pull-right" href="#dte_meterread_modal" role="button" data-toggle="modal"title="Next Meter Read Date"><i class="icon-question-sign"></i></a>
								<span class="help-inline lead" id="dte_meterread_error" style="display: none;"><em>Your Next Meter Read Date is required</em></span>
							</div>
						</div>
						
						</fieldset>
						
						<fieldset>
							<legend>Personal Information</legend>
							
							<div class="control-group" id="company_group">
								<label class="description" for="Comp_Name">Company Name</label> <!-- completed -->
								<input type="text" value="<?php echo set_value('Comp_Name'); ?>" name="Comp_Name" id="Comp_Name" class="input-xlarge" rel="popover">
								<span class="help-inline lead" id="company_error" style="display: none;"><em>Company name is required for non-residential accounts</em></span>
							</div>
							
							<!-- <input type="text" id="authorized_person" class="input-xlarge" rel="popover"> -->
							<div class="control-group" id="authorized_person" style="width: 90%;"> <!-- completed -->
								<label class="description" for="Cust_F_Name">Account Holder or Legally Authorized Person</label>
								
								<input class="input-medium" name="Cust_F_Name" value="<?php echo set_value('Cust_F_Name'); ?>" id="first_name" type="text" placeholder="First">
								<input class="input-mini" name="Cust_MI" value="<?php echo set_value('Cust_MI'); ?>" id="middle_initial" type="text" placeholder="MI">
								<input class="input-medium" name="Cust_L_Name" value="<?php echo set_value('Cust_L_Name'); ?>" id="last_name" type="text" placeholder="Last">
								<span class="help-inline lead" id="name_error" style="display: none;"><em>Account holder's name is required</em></span>
							</div>
							
							<div class="control-group" id="phone_group"> <!-- completed -->
								<label class="description" for="Phone1">Phone Number</label>
								<div class="controls">
									<div class="input-prepend">
										<span class="add-on"><i class="icon-bell"></i></span>
										<input  type="text" name="Phone1" value="<?php echo set_value('Phone1'); ?>" id="Phone1">
									</div>
								</div>
								<span class="help-inline lead" id="phone_error" style="display: none;"><em>A contact phone number is required</em></span>
							</div>
							
							<div class="control-group" id="email_group"> <!-- completed -->
								<label class="description" for="Email">Email Address</label>
								<div id="email_div" class="controls" style="width: 55%;">
									<div class="input-prepend">
										<span class="add-on"><i class="icon-envelope"></i></span>
										<input type="text" name="Email" value="<?php echo set_value('Email'); ?>" id="Email" placeholder="user@domain" rel="popover">
									</div>
								</div>
								<span class="help-inline lead" id="email_error" style="display: none;">A valid email address is required</span>
							</div>
						</fieldset>
						
						<fieldset>
						<legend>Address Information</legend>
						
						<label class="description" for="element_2">Service Address</label>
						
						<!-----Dummy Address----->
						<span class="dummy">
						<input type="text" id="dummy_service_address" placeholder="Select a Natural Gas Utility Above" class="input-xlarge" disabled>
						<br />
						<input class="input-medium" type="text" id="dummy_service_city" placeholder="City" disabled> <input class="input-mini" value="MI" type="text" id="dummy_service_state" placeholder="MI" disabled> <input class="input-small" type="text" id="dummy_service_zip" placeholder="5 digit zip" disabled>
						</span>
						<!----------------------->
						
						<!---Consumers Service Address--->
						<span class="consumers"> <!-- not completed -->
							<div class="control-group" id="con_service_address_group">
							<input type="text" name="con_service_street" value="<?php echo set_value('con_service_street'); ?>" id="con_service_street" placeholder="Street Address"class="input-xlarge" rel="popover"><a class="btn btn-small pull-right" href="#con_serv_addr_modal" role="button" data-toggle="modal" title="Service Address"><i class="icon-question-sign"></i></a>
							<br />
							<input class="input-medium" type="text" name="con_service_city" value="<?php echo set_value('con_service_city'); ?>" id="con_service_city"placeholder="City"> <input class="input-mini" value="MI" type="text" id="con_service_state" placeholder="MI" disabled> <input class="input-small" type="text" name="con_service_zip" value="<?php echo set_value('con_service_zip'); ?>" id="con_service_zip" placeholder="5 digit zip">
							<span class="help-inline lead" id="con_service_address_error" style="display: none;">A valid email address is required</span>
							</div>
						</span>
						<!----------------------->
						
						<!--DTE Service Address-->
						<span class="dte"> <!-- not completed -->
							<div class="control-group" id="dte_service_address_group">
							<input type="text" name="Serv_Street_Number" value="<?php echo set_value('Serv_Street_Number'); ?>" id="Serv_Street_Number" placeholder="House #" class="input-mini" rel="popover"> <input type="text" name="dte_service_street" value="<?php echo set_value('dte_service_street'); ?>" id="dte_service_street" placeholder="Street" class="input-xlarge" rel="popover"><a class="btn btn-small pull-right" href="#dte_serv_addr_modal" role="button" data-toggle="modal" title="Service Address"><i class="icon-question-sign"></i></a>
							<br />
							<input class="input-medium" type="text" name="dte_service_city" value="<?php echo set_value('dte_service_city'); ?>" id="dte_service_city" placeholder="City"> <input class="input-mini" value="MI" type="text" id="dte_service_state" placeholder="MI" disabled> <input class="input-small" type="text" name="dte_service_zip" value="<?php echo set_value('dte_service_zip'); ?>" id="dte_service_zip" placeholder="5 digit zip">
							<span class="help-inline lead" id="dte_service_address_error" style="display: none;">A valid email address is required</span>
							</div>
						</span>
						<!----------------------->
						
						<p />
						<p />
						<label class="description form-inline" for="element_2">Mailing Address</label> <label class="checkbox"><input type="checkbox" name="service_mail" id="service_mail" value="TRUE">Mailing and service addresses are the same.</label>
						
						<div id="maildiv">
							<div class="control-group" id="mail_address_group">
							<input type="text" name="Mail_Address" value="<?php echo set_value('Mail_Address'); ?>" id="Mail_Address" placeholder="Street Address" class="input-xlarge form-inline"><br />
							<input type="text" name="Mail_City" value="<?php echo set_value('Mail_City'); ?>" id="Mail_City" class="input-medium" type="text" placeholder="City">
							<input type="text" name="Mail_State" value="<?php echo set_value('Mail_State'); ?>" id="Mail_State" class="input-mini" type="text" placeholder="State">
							<input type="text" name="Mail_Zip" value="<?php echo set_value('Mail_Zip'); ?>" id="Mail_Zip" class="input-small" type="text" placeholder="5 digit zip">
							<span class="help-inline lead" id="mail_address_error" style="display: none;">A valid email address is required</span>
							</div>
						</div>
						</fieldset>
						<legend></legend>
						<textarea name="Comments" class="field span6" id="Comments" placeholder="Share your comments here..." rows="4"><?php echo set_value('Comments'); ?></textarea>
						
						<div class="row">
							<div class="span8">
								<legend>Acknowledgement</legend>
									<div>
									<p id="acknowledgement">
									</p>
									</div>
								<legend></legend> <!-- Sets an HR effect when no text is input -->
							</div>
						</div>
						
						<br />
						<input class="btn btn-primary button" id="button" type="submit" value="Submit" />
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		
		
		
		
		<div class="footer">
			<p>&copy; Company <?php echo date('Y');?></p>
		</div>

	</div> <!-- /container -->




<!-- Service Type Modal -->
<div id="service_type_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Service Type</h3>
  </div>
  <div class="modal-body">
    <p>The service type will determine what contract you will enter into.  If you are a non-commercial residence then the choice is simply Residential.</p>
	<p>If you are a commercial user then your contract is determined by your annual usage, either above or below 500 mcf.  Your utility bill may have more information for determining this value.</p>
	<h4>Please select your utility on the form for further assistance.</h4>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>


<div id="con_service_type_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Service Type</h3>
  </div>
  <div class="modal-body">
    <p>The service type will determine what contract you will enter into.  If you are a non-commercial residence then the choice is simply Residential.</p>
	<p>If you are a commercial user then your contract is determined by your annual usage, either above or below 500 mcf.</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><img class="img-polaroid" src="<?php echo base_url();?>img/assistance/servicetype.png" /></td>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/servicetype_overview.png" /></td>
			</tr>
			<tr>
				<td colspan = "2" align="center">These are the total usage for each month. Total each month while not including the first which on the chart.</td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>


<div id="dte_service_type_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Service Type</h3>
  </div>
  <div class="modal-body">
    <p>The service type will determine what contract you will enter into.  If you are a non-commercial residence then the choice is simply Residential.</p>
	<p>If you are a commercial user then your contract is determined by your annual usage, either above or below 500 mcf.</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/graph.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!------------------------>

<!-- Service Name Modal -->
<div id="con_serv_name_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Service Name</h3>
  </div>
  <div class="modal-body">
    <p>The service name may or may not be the same as listed at the top of the bill in the mailing address section.  This needs to be exactly the same as it is listed
	on your bill, including any mispellings or typos on Consumers Energy's behalf.
	</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><img class="img-polaroid" src="<?php echo base_url();?>img/assistance/servicename.png" /></td>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/servicename_overview.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="dte_serv_name_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Service Name</h3>
  </div>
  <div class="modal-body">
    <p>The service name may or may not be the same as listed at the top of the bill in the mailing address section.  This needs to be exactly the same as it is listed
	on your bill, including any mispellings or typos on DTE's behalf.
	</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/top-small.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!------------------------>

<!-- Account Number Modal -->
<div id="con_account_number_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Account Number</h3>
  </div>
  <div class="modal-body">
    <p>
	Your Consumers Energy account number is located in the right hand column in the Summary of Charges section of your bill.  
	In certain circumstances your service address can differ from your mailing address.  These would be in appartment situations 
	with different numbering schemes and commercial situations.  It is important to double check that you have the correct address.
	</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><img class="img-polaroid" src="<?php echo base_url();?>img/assistance/servicename.png" /></td>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/servicename_overview.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="dte_account_number_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Account Number</h3>
  </div>
  <div class="modal-body">
    <p>One fine body�</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><h3>No Images Available</h3></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!------------------------>

<!-- Meter Modal -->
<div id="con_meter_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Natural Gas POD Number</h3>
  </div>
  <div class="modal-body">
    <p>
	This number is very important as it identifies your natural gas meter.  
	It is also the easiest to make a mistake on.  You actually have two POD numbers from Consumers Energy, one natural gas and one electric.  
	For this reason please make sure you record the POD number under the Gas service section of your bill.
	</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><img class="img-polaroid" src="<?php echo base_url();?>img/assistance/pod.png" /></td>
				<td align="center"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/pod_overview.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="dte_meter_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Natural Gas Meter Number</h3>
  </div>
  <div class="modal-body">
    <p>One fine body�</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><h3>No Images Available</h3></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="dte_meterread_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Next Meter Read Date</h3>
  </div>
  <div class="modal-body">
    <p>One fine body�</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><h3>No Images Available</h3></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!------------------------>

<!-- Service Address Modal -->
<div id="con_serv_addr_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Consumers Service Address</h3>
  </div>
  <div class="modal-body">
    <p>
	Service address located in the right hand column in the Summary of Charges section.  
	In certain circumstances your service address can differ from your mailing address.  
	It is important to check that you have the correct service address.
	</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><img class="img-polaroid" src="<?php echo base_url();?>img/assistance/servicename.png" /></td>
				<td align="center" valign="top"><img class="img-polaroid pull-right" src="<?php echo base_url();?>img/assistance/servicename_overview.png" /></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="dte_serv_addr_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">DTE Service Address</h3>
  </div>
  <div class="modal-body">
    <p>One fine body�</p>
	<h4>Use the following images as refference</h4>
	<hr />
	<table align="center" style="margin-left: auto; margin-right: auto; width: 100%;" border="0">
		<tbody>
			<tr>
				<td align="center"><h3>No Images Available</h3></td>
			</tr>
		</tbody>
	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!------------------------>


<script>  
//////////PopOvers//////////////////////////
//service_name popover code
$(function ()
{ $("#con_service_name").popover({title: 'Service Name', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});  
});

$(function ()
{ $("#dte_service_name").popover({title: 'Service Name', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});  
});

//account number popover code
$(function ()
{ $("#con_account_number").popover({title: 'Account Number', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});  
});

$(function ()
{ $("#dte_account_number").popover({title: 'Account Number', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});
});

//service_street popover code
$(function ()
{ $("#con_service_street").popover({title: 'Service Address', content: "This is the address listed with your Service Name and may be different than the mailing address.", trigger: 'hover', placement: 'right'});
});

$(function ()
{ $("#dte_service_number").popover({title: 'Service Address', content: "This is the address listed with your Service Name and may be different than the mailing address.", trigger: 'hover', placement: 'right'});
});

//meter popover code
$(function ()
{ $("#con_meter").popover({title: 'Natural Gas POD Number', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});
});

$(function ()
{ $("#dte_meter").popover({title: 'Natural Gas Meter Number', content: "This may not be the same as your mailing name on the top tear off section on your bill.", trigger: 'hover', placement: 'right'});
});

//authorized_person popover code
$(function ()
{ $("#authorized_person").popover({title: 'Account Holder', content: "If your name is different from the service name, or you are authorized and acting on the account holders behalf.", trigger: 'hover', placement: 'right'});
});

//email popover code
$(function ()
{ $("#email_div").popover({title: 'Email Address', content: "This must be a valid email address because a confirmation email is used to confirm your identity.", trigger: 'hover', placement: 'right'});
});
////////////////////////////////////////////


//account type detection / reaction
$(document).ready(function(){
    
	
	
	//Detect whether or not there are errors and style accordingly
	$("val_errors").hide();
	var span_content = $("div#val_errors p").html();
	var title = '<button type="button" class="close" data-dismiss="alert">&times;</button><h4>Warning!</h4>';
	
	//alert(span_content);
	if(span_content != null)
		{
		$("#val_errors").html(title + span_content);
		//alert(span_content);
		$("#val_errors").show();
		}
	
	//set initial view of consumers and dte classes as hidden
	$(".dte").hide();
	$(".consumers").hide();
	
	
	//based on the value of the utility drop down different fields appear
	//set the value of #service_type
	var n = $("#Utility").val();
		
	//if the account type is residential, grey out company name text input
	if (n == 'consumers')
		{
		$(".dte").hide();
		$(".consumers").fadeIn(1000);
		$(".dummy").hide();
		}
	else if (n == 'dte')
		{
		$(".dte").fadeIn(1000);
		$(".consumers").hide();
		$(".dummy").hide();
		}
	else
		{
		$(".dte").hide();
		$(".consumers").hide();
		$(".dummy").show();
		}
	
	$('#Utility').change(function() 
		{
		//set the new value of #service_type
		var n = $("#Utility").val();
		
		//if the account type is residential, grey out company name text input
		if (n == 'consumers')
			{
			$(".dte").hide();
			$(".consumers").fadeIn(1000);
			$(".dummy").hide();
			}
		else if (n == 'dte')
			{
			$(".dte").fadeIn(1000);
			$(".consumers").hide();
			$(".dummy").hide();
			}
		else
			{
			$(".dte").hide();
			$(".consumers").hide();
			$(".dummy").show();
			}
		});

		
		
	//set the initial value of #service_type
	var n = $("#service_type").val();
	
	//if the account type is residential, grey out company name text input
	if (n == 'residential')
		{
		$("#Comp_Name").attr('readonly', 'readonly');
		$("#Comp_Name").attr('placeholder', 'Not applicable for residential accounts');
		$("#Comp_Name").addClass('uneditable-input');
		}
	//if it isn't a residential un-grey out company name text input
	else
		{
		$("#Comp_Name").removeAttr('readonly');
		$("#Comp_Name").removeClass('uneditable-input');
		$("#Comp_Name").attr('placeholder', '');
		}
		
		
	$('#Cust_Type').change(function()
		{
		//set the new value of #service_type
		var n = $("#Cust_Type").val();
		
		//if the account type is residential, grey out company name text input
		if (n == 'residential')
			{
			$("#Comp_Name").attr('readonly', 'readonly');
			$("#Comp_Name").attr('placeholder', 'Not applicable for residential accounts');
			$("#Comp_Name").addClass('uneditable-input');
			}
		//if it isn't a residential un-grey out company name text input
		else
			{
			$("#Comp_Name").removeAttr('readonly');
			$("#Comp_Name").removeClass('uneditable-input');
			$("#Comp_Name").attr('placeholder', '');
			}
		});
		
		
		//Set the initial value and state of mailAddr
		var mailAddr = $("#service_mail").attr('checked');
		if(mailAddr == 'checked')
			{
			$("#mail_street").attr('readonly', 'readonly');
			$("#mail_city").attr('readonly', 'readonly');
			$("#mail_state").attr('readonly', 'readonly');
			$("#mail_zip").attr('readonly', 'readonly');
			$("#maildiv").slideToggle();
			}
		else
			{
			$("#mail_street").removeAttr('readonly');
			$("#mail_city").removeAttr('readonly');
			$("#mail_state").removeAttr('readonly');
			$("#mail_zip").removeAttr('readonly');
			$("#maildiv").show();
			}
		
		$('#service_mail').change(function()
		{
		var mailAddr = $("#service_mail").attr('checked');
		if(mailAddr == 'checked')
			{
			$("#mail_street").attr('readonly', 'readonly');
			$("#mail_city").attr('readonly', 'readonly');
			$("#mail_state").attr('readonly', 'readonly');
			$("#mail_zip").attr('readonly', 'readonly');
			$("#maildiv").slideToggle();
			}
		else
			{
			$("#mail_street").removeAttr('readonly');
			$("#mail_city").removeAttr('readonly');
			$("#mail_state").removeAttr('readonly');
			$("#mail_zip").removeAttr('readonly');
			$("#maildiv").slideToggle();
			}
		});
		
		jQuery(	function($)
			{
				$("#Phone1").mask("(999) 999-9999",{placeholder:" "});
			});
	});

		jQuery(function()
			{
			jQuery.support.placeholder = false;
			test = document.createElement('input');
			if('placeholder' in test) jQuery.support.placeholder = true;
			});
			
			
		 $(function()
			{
			if(!$.support.placeholder)
				{
				var active = document.activeElement;
				$(':text').focus(function ()
					{
					if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) 
						{
						$(this).val('').removeClass('hasPlaceholder');
						}
					}).blur(function () 
						{
						if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) 
							{
							$(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
							}
						});
					$(':text').blur();
					$(active).focus();
					$('form').submit(function () 
						{
						$(this).find('.hasPlaceholder').each(function() 
							{ $(this).val(''); });
						});
				}
			});
</script>