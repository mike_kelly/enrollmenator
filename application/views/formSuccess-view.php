<body>
	<div class="container-narrow" style="align: center;">
		
		<div class="row">
			<div id="value"></div>
			<div class="span8">
				<h2>Bishop Energy Online Enrollment</h2>
				<hr />
				<h3>Submission Successful!</h3>
			</div>
			<div class="span8">
				<p>
				<?php echo $programTerms;?>
				</p>
				<p><?php echo $confirmURL;?></p>
				<p>
				<?php echo $acknowledgement;?>
				</p>
			</div>
		</div>
		<div class="footer">
			<p>&copy; Company <?php echo date('Y');?></p>
		</div>
	</div> <!-- /container -->