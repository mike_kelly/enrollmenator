    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery.maskedinput-1.3.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="<?php echo base_url();?>js/<?php echo $script;?>"></script>
  </body>
</html>