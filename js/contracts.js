//Manage the prep work and data formatting so contracts can be generated
function toTitleCase(str)
			{
			return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
			}

$(document).ready(function(){
	
	//validation reset
	$('.control-group').removeClass('error');
	$('.alert').hide();
	
	//initial UI Setup
	$('#custInfo').hide();
	$('#batchReport').hide();
	
	
	var ajax_load = "<img src='http://bishopenergy.com/online/img/loading.gif' alt='loading...' />";  
	var sessionID = $('#sessionID').val();
	var loadUrl = "http://bishopenergy.com/online/ajax/datapull/" + sessionID; 
	
	
	//an ajax transaction that determines if there are batch jobs and if #batchReport is displayed
	//character count is a really crappy way to determine this because small result edits break it
	//a better determination would be a count of the <tr> elements e.g. > 1 <tr>
	$("#batchReport").html(ajax_load).load(loadUrl, null, function(responseText)
			{
            characterCount = responseText.length;
			if(characterCount > 594){$('#batchReport').slideDown();}
			});
	
	
	//the html contents of #addinfo determines if #custInfo is displayed or not
	//again this is a crappy way of making this determination can cause chaining errors
	$addTag = $('#addinfo').html();
	
	var Date = '';
	
	//Change the date dropdown option based on the utility choice
	$('#Utility').change(function()
		{
		utility = $("#Utility").val();
		switch(utility)
			{
			case "Consumers":
				$("#Date").hide()
				$("#Date").prop('disabled', true);
				$("#dteDate").hide();
				$("#dteDate").prop('disabled', true);
				$("#consumersDate").fadeIn();
				$("#consumersDate").prop('disabled', false);
				Date = $('select#consumersDate option:selected').val();
				break;
				
			case "DTE":
				$("#Date").hide();
				$("#Date").prop('disabled', true);
				$("#dteDate").fadeIn();
				$("#dteDate").prop('disabled', false);
				$("#consumersDate").hide();
				$("#consumersDate").prop('disabled', true);
				Date = $('select#dteDate option:selected').val();
				break;
				
			default:
				$("#Date").fadeIn();
				$("#Date").prop('disabled', false);
				$("#dteDate").hide();
				$("#dteDate").prop('disabled', true);
				$("#consumersDate").hide();
				$("#consumersDate").prop('disabled', true);
				Date = '0000-00-00';
				break;
			}
		
		});
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$('#addinfo').click(function()
		{
		// set the html data to the $addTag variable
		$addTag = $('#addinfo').html();
		
		// slide animation to show the customer info form
		$('#custInfo').slideToggle();
		
		// as the animation toggles, the html data of $addTag also toggles
		if($addTag == '<i class="icon-plus"></i> Add Customer Info')
			{
			// change text of info button
			$('#addinfo').html('<i class="icon-minus"></i> Remove Customer Info');
			
			$('#populatedState').val('1');
			
			// change radio to contract
			$('input:radio[name=DocType]').filter('[value=Contract]').prop('checked', true);
			$('input:radio[name=DocType]').filter('[value=Terms]').prop('disabled', true);
			}
		else
			{
			$('#addinfo').html('<i class="icon-plus"></i> Add Customer Info');
			$('#populatedState').val('0');
			$('input:radio[name=DocType]').filter('[value=Terms]').prop('disabled', false);
			}
		});
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//click the plus button to add document to batch
	$('#addButton').click(function()
		{
		//contract type variables
		Utility = $('select#Utility option:selected').val();
		AccType = $('select#AccType option:selected').val();
		DocType = $('input:radio[name=DocType]:checked').val();
		$addTag = $('#addinfo').html();
		
		//if selections have not been made display error information
		if(Utility == '0' || AccType == '0' || typeof DocType == 'undefined')
			{
			$('.control-group').addClass('error');
			$('.alert').show();
			}
		
		else
			{
			//Reset error displays
			$('.control-group').removeClass('error');
			$('.alert').hide();
			
			
			//Collect all form information
			//contract type variables
			Utility = $('select#Utility option:selected').val();
			AccType = $('select#AccType option:selected').val();
			DocType = $('input:radio[name=DocType]:checked').val();
			
			
			AccNumber = $('#AccNumber').val();
			MeterNumber = $('#MeterNumber').val();
			
			// Contact Information
			FirstName = $('#FirstName').val();
			LastName = $('#LastName').val();
			SrvName = $('#SrvName').val();
			CompName = $('#CompName').val();
			
			Email = $('#Email').val();
			Phone = $('#Phone').val();
			Fax = $('#Fax').val();
			
			// Service Address
			SrvStrNumber = $('#SrvStrNumber').val();
			SrvStrName = $('#SrvStrName').val();
			SrvCity = $('#SrvCity').val();
			SrvState = $('#SrvState').val();
			SrvZip = $('#SrvZip').val();
			
			// Mailing Address
			MailStreet = $('#MailStreet').val();
			MailCity = $('#MailCity').val();
			MailState = $('#MailState').val();
			MailZip = $('#MailZip').val();
			
			
			
			
			//grab value of all the inputs and assign to variables
			if($addTag == '<i class="icon-plus"></i> Add Customer Info')
				{
				populated = 0;
				}
			else if($addTag =='<i class="icon-minus"></i> Remove Customer Info')
				{
				populated = 1;
				}
			
			
			//change case to the appropriate case for the
			Email = Email.toLowerCase();
			SrvStrName = toTitleCase(SrvStrName);
			CompName = toTitleCase(CompName);
			SrvCity = toTitleCase(SrvCity);
			SrvState = SrvState.toUpperCase();
			MailStreet = toTitleCase(MailStreet);
			MailCity = toTitleCase(MailCity);
			MailState = MailState.toUpperCase();
			
			FirstName = $('#FirstName').val();
			LastName = $('#LastName').val();
			SrvName = $('#SrvName').val();
			CompName = $('#CompName').val();
			
			sessionID = $('#sessionID').val();
			
			//save the dropdown date value that isn't disabled
			if(!$('#Date').is(':disabled'))
				{Date = '0000-00-00';}
				
			if(!$('#consumersDate').is(':disabled'))
				{Date = $('select#consumersDate option:selected').val();}
				
			if(!$('#dteDate').is(':disabled'))
				{Date = $('select#dteDate option:selected').val();}
			
			
			
			// add variables to datastring
			var dataString = 'Date=' + Date + '&Utility=' + Utility + '&sessionID=' + sessionID + '&AccType=' + AccType + '&DocType=' + DocType + '&populated=' + populated + '&AccNumber=' + AccNumber + '&MeterNumber=' + MeterNumber + '&FirstName=' + FirstName + '&LastName=' + LastName + '&SrvName=' + SrvName + '&CompName=' + CompName + '&Email=' + Email + '&Phone=' + Phone + '&Fax=' + Fax + '&SrvStrNumber=' + SrvStrNumber + '&SrvStrName=' + SrvStrName + '&SrvCity=' + SrvCity + '&SrvState=' + SrvState + '&SrvZip=' + SrvZip + '&MailStreet=' + MailStreet + '&MailCity=' + MailCity + '&MailState=' + MailState + '&MailZip=' + MailZip;
			
			$.ajax({
				type: 	"POST",  
				url: 	"http://bishopenergy.com/online/ajax/",
				data: 	dataString,  
				success: function() 
					{
					// Set fields back to zero state
					// selects and radios
					$('select#Utility>option:eq(0)').prop('selected', true);
					$('select#AccType>option:eq(0)').prop('selected', true);
					
					// $("#Date").fadeIn().removeAttr('disabled',false);
					// $("#dteDate").hide().attr('disabled',true);
					// $("#consumersDate").hide().attr('disabled',true);
					
					$('input:radio[name=DocType]').filter('[value=contract]').prop('checked', false);
					$('input:radio[name=DocType]').filter('[value=terms]').prop('checked', false);
					
					//hide() customer info div
					$('#custInfo').slideUp();
						//because of a crappy planned state detection of whether #custInfo is
						//visible or hidden I have to do this crap
						//state detection is based off of the html content of #addinfo
					if($addTag != '<i class="icon-plus"></i> Add Customer Info')
						{
						$('input:radio[name=DocType]').filter('[value=terms]').prop('disabled', false);
						$('#addinfo').html('<i class="icon-plus"></i> Add Customer Info');
						}
					
					//input fields
						//Account Information
					$('input#AccNumber').val('');
					$('input#MeterNumber').val('');
						//Contact Information
					$('input#FirstName').val('');
					$('input#LastName').val('');
					$('input#SrvName').val('');
					$('input#CompName').val('');
					
					$('input#Email').val('');
					$('input#Phone').val('');
					$('input#Fax').val('');
						//Service Address
					$('input#SrvStrNumber').val('');
					$('input#SrvStrName').val('');
					$('input#SrvCity').val('');
					$('input#SrvZip').val('');
						//Mailing Address
					$('input#MailStreet').val('');
					$('input#MailCity').val('');
					$('input#MailState').val('');
					$('input#MailZip').val('');
					
					//run subsequent ajax call to load bath table html
					//$.ajaxSetup 
					//	({cache: false});  
					var ajax_load = "<img src='http://bishopenergy.com/online/img/loading.gif' alt='loading...' />";
					//  load() functions
					var loadUrl = "http://bishopenergy.com/online/ajax/datapull/" + sessionID;
					
					$("#batchReport").html(ajax_load).load(loadUrl, null, function(responseText){
						//Success function from the ajax exchange
						//selects the last row in the batch table and gives it the .success class
						//then waits three seconds and removes that class
						//helps identify the newely added record
						characterCount = responseText.length;
						$('tr:first-child','#ajaxTable').addClass('success');
						setTimeout(function(){$('tr').removeClass('success')},2500);
							}).fadeIn();
							
					} // end of the first ajax success function
				}); //end of the ajax function
			
			
			}
		
		}); // end of the addbutton.click function
		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
	}); // end of document.ready
	