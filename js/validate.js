
$(document).ready(function()
	{
	//jqueryUI datepicker
	$( "#Meter_Read" ).datepicker({ showAnim: "fold", dateFormat: "yy-mm-dd" });
    
	//This section is for the ajax call to change out the program terms and acknowledgement sections of the form
	//All of this should go into it's own
	var utility = $("#Utility").val();
	var serv_type = $("#Cust_Type").val();
	var data = "";
	$(".maindrops").change(function()
		{
		utility = $("#Utility").val();
		serv_type = $("#Cust_Type").val();
		if(utility != "" && serv_type != "")
			{
			var url = "http://bishopenergy.com/online/api/proginfo_get/" + utility +"/" + serv_type;
			var jsonp = '[{"Lang":"jQuery","ID":"1"},{"Lang":"C#","ID":"2"}]';
			$.getJSON(url,function(json)
				{
				//alert(json[0].Utility + " " + json[0].ContractType);
				$("#terms").html(json[0].ProgramTerms.replace("\n", "</p><p>", "g"));
				
				$("#acknowledgement").html(json[0].Acknowledgement.replace("\n", "</p><p>", "g"));
				
				//Rebuild the program terms and the acknowledgement sections of the contract based on the the json response	
				//I believe it is a good idea to put all the contract verbage into the database and stop taking just the different
				//sections.  When changes are made this could cause lots of problems grammer wise.
				});
			}
		else{
			$("#terms").html("");
			$("#acknowledgement").html("");
			}
		});
	
	$("#button").click(function()
		{
		//Reset all errors
		
		$("#utility_group").removeClass("error");
		$("#utility_error").hide();
		
		$("#service_group").removeClass("error");
		$("#service_error").hide();

		$("#con_service_name_group").removeClass("error");
		$("#con_service_name_error").hide();

		$("#dte_service_name_group").removeClass("error");
		$("#dte_service_name_error").hide();
		
		$("#con_account_number_group").removeClass("error");
		$("#con_account_number_error").hide();
		
		$("#dte_account_number_group").removeClass("error");
		$("#dte_account_number_error").hide();
		
		$("#con_meter_group").removeClass("error");
		$("#con_meter_error").hide();
		
		$("#dte_meter_group").removeClass("error");
		$("#dte_meter_error").hide();

		$("#company_group").removeClass("error");
		$("#company_error").hide();
		
		$("#authorized_person").removeClass("error");
		$("#name_error").hide();
		
		$("#phone_group").removeClass("error");
		$("#phone_error").hide();
		
		$("#email_group").removeClass("error");
		$("#email_error").hide();
		
		$("#con_service_address_group").removeClass("error");
		$("#con_service_address_error").hide();
		
		$("#dte_service_address_group").removeClass("error");
		$("#dte_service_address_error").hide();
		
		
		var emailPattern = new RegExp(/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/);
		var numericPattern = new RegExp(/[0-9]/);
		
		
		// validate and process form here
		var utility = $("#Utility").val();
		if(utility == "")
			{
			$("#Utility").focus();
			$("#utility_group").addClass("error");
			$("#utility_error").show();
			return false;
			}
		
		var serv_type = $("#Cust_Type").val();
		if(serv_type == "")
			{
			$("#Cust_Type").focus();
			$("#service_group").addClass("error");
			$("#service_error").show();
			return false;
			}
		
		//Consumers utility information checks serviceName, accountNumber, meterNumber//////////////
		if(utility == "consumers")
			{
			var con_service_name = $("#con_service_name").val();
			var con_account_number = $("#Consumers_Acc_Number").val();
			var con_meter_number = $("#POD_Number").val();
			
			if(con_service_name == "")
				{
				$("#con_service_name").focus();
				$("#con_service_name_group").addClass("error");
				$("#con_service_name_error").show();
				return false;
				}
			
			if(con_account_number == "")
				{
				$("#Consumers_Acc_Number").focus();
				$("#con_account_number_group").addClass("error");
				$("#con_account_number_error").html("The Account Number is a required field");
				$("#con_account_number_error").show();
				return false;
				}
			if (con_account_number != "" && !con_account_number.match(numericPattern))
				{
				$("#Consumers_Acc_Number").focus();
				$("#con_account_number_group").addClass("error");
				$("#con_account_number_error").html("A valid account number contains only numbers");
				$("#con_account_number_error").show();
				return false;
				}
			
			if(con_meter_number == "")
				{
				$("#POD_Number").focus();
				$("#con_meter_group").addClass("error");
				$("#con_meter_error").html("The POD Number is a required field");
				$("#con_meter_error").show();
				return false;
				}
			if (con_meter_number != "" && !con_meter_number.match(numericPattern))
				{
				$("#POD_Number").focus();
				$("#con_meter_group").addClass("error");
				$("#con_meter_error").html("A valid POD contains only numbers");
				$("#con_meter_error").show();
				return false;
				}
			}
		////////////////////////////////////////////////////////////////////////////////////////////
		
		//DTE utility information checks serviceName, accountNumber, meterNumber////////////////////
		else if(utility == "dte")
			{
			var dte_service_name = $("#dte_service_name").val();
			var dte_account_number = $("#DTE_Acc_Number").val();
			var dte_meter_number = $("#Meter_Number").val();
			var dte_meter_date = $("#Meter_Read").val();
			
			if(dte_service_name == "")
				{			
				$("#dte_service_name").focus();
				$("#dte_service_name_group").addClass("error");
				$("#dte_service_name_error").show();
				return false;
				}
			
			if(dte_account_number == "")
				{
				$("#DTE_Acc_Number").focus();
				$("#dte_account_number_group").addClass("error");
				$("#dte_account_number_error").html("The Account Number is a required field");
				$("#dte_account_number_error").show();
				return false;
				}
			if(dte_account_number != "" && !dte_account_number.match(numericPattern))
				{
				$("#DTE_Acc_Number").focus();
				$("#dte_account_number_group").addClass("error");
				$("#dte_account_number_error").html("A valid account number contains only numbers");
				$("#dte_account_number_error").show();
				return false;
				}
			
			if(dte_meter_number == "")
				{
				$("#Meter_Number").focus();
				$("#dte_meter_group").addClass("error");
				$("#dte_meter_error").html("The Meter Number is a required field");
				$("#dte_meter_error").show();
				return false;
				}
			if(dte_meter_number != "" && !dte_meter_number.match(numericPattern))
				{
				$("#Meter_Number").focus();
				$("#dte_meter_group").addClass("error");
				$("#dte_meter_error").html("A valid Meter Number contains only numbers");
				$("#dte_meter_error").show();
				return false;
				}
			var dte_meter_date = $("#Meter_Read").val();
			if(dte_meter_date == "")
				{
				$("#Meter_Read").focus();
				$("#dte_meterread_group").addClass("error");
				$("#dte_meterread_error").html("You must supply your next meter read date.");
				$("#dte_meterread_error").show();
				return false;
				}
			}
		////////////////////////////////////////////////////////////////////////////////////////////
		
		var serviceType = $("#Cust_Type").val();
		var companyName = $("#Comp_Name").val();
		
		if (serviceType != "residential")
			{
			if(companyName == "")
				{
				$("#dte_meter").focus();
				$("#company_group").addClass("error");
				$("#company_error").html("Company Name is required for Commercial Accounts");
				$("#company_error").show();
				return false;
				}
			}
		
		var firstname = $("#Cust_F_Name").val();
		var lastname = $("#Cust_L_Name").val();
		
		if (firstname == "")
			{
			//Also add the error class to the input set
			//$("label#name_error").show();
			$("#Cust_F_Name").focus();
			$("#authorized_person").addClass("error");
			$("#name_error").html("Account holder's first name is required");
			$("#name_error").show();
			return false;
			}
		else if (lastname == "")
			{
			$("#Cust_L_Name").focus();
			$("#authorized_person").addClass("error");
			$("#name_error").html("Account holder's last name is required");
			$("#name_error").show();
			return false;
			}
			
		var phone = $("input#phone").val();
		if (phone == "")
			{
			$("input#phone").focus();
			$("#phone_group").addClass("error");
			$("#phone_error").show();
			return false;
			}
		
		var email = $("#email").val();
		var repCode = $("#repCode").val();
		if(repCode == "")
			{
			if (!email.match(emailPattern))
				{
				var error_message = "This is the variable message";
				$("#email").focus();
				$("#email_group").addClass("error");
				$("#email_error").html(error_message);
				$("#email_error").show();
				return false;
				}
			}
		//street
		var con_service_street = $("#con_service_street").val();
		//city
		var con_service_city = $("#con_service_city").val();
		//zip
		var con_service_zip = $("#con_service_zip").val();
		
		if (utility == "consumers")
			{
			if(con_service_street == "")
				{
				var error_message = "The street address is a required field";
				$("#con_service_street").focus();
				$("#con_service_address_group").addClass("error");
				$("#con_service_address_error").html(error_message);
				$("#con_service_address_error").show();
				return false;
				}
			if(con_service_city == "")
				{
				var error_message = "The city is required for the service address";
				$("#con_service_city").focus();
				$("#con_service_address_group").addClass("error");
				$("#con_service_address_error").html(error_message);
				$("#con_service_address_error").show();
				return false;
				}
			if(con_service_zip == "")
				{
				var error_message = "A 5 digit zip code is required";
				$("con_service_zip").focus();
				$("#con_service_address_group").addClass("error");
				$("#con_service_address_error").html(error_message);
				$("#con_service_address_error").show();
				return false;
				}
			if(con_service_zip != "" && !con_service_zip.match(numericPattern))
				{
				var error_message = "A valid 5 digit must contain only numbers";
				$("con_service_zip").focus();
				$("#con_service_address_group").addClass("error");
				$("#con_service_address_error").html(error_message);
				$("#con_service_address_error").show();
				return false;
				}
			}
		//street number
		var dte_service_number = $("#Serv_Street_Number").val();
		//street name
		var dte_service_street = $("#dte_service_street").val();
		//city
		var dte_service_city = $("#dte_service_city").val();
		//zip
		var dte_service_zip = $("#dte_service_zip").val();
			
		if (utility == "dte")
			{
			if(dte_service_number == "")
				{
				var error_message = "The street number is a required field";
				$("#Serv_Street_Number").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			if(dte_service_number != "" && !dte_service_number.match(numericPattern))
				{
				var error_message = "The street number must be only numbers";
				$("#Serv_Street_Number").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			if(dte_service_street == "")
				{
				var error_message = "The street address is a required field";
				$("#dte_service_street").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			if(dte_service_city == "")
				{
				var error_message = "The city is required for the service address";
				$("#dte_service_city").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			if(dte_service_zip == "")
				{
				var error_message = "A 5 digit zip code is required";
				$("#dte_service_zip").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			if(dte_service_zip != "" && !dte_service_zip.match(numericPattern))
				{
				var error_message = "A valid 5 digit zip code must contain only numbers";
				$("#dte_service_zip").focus();
				$("#dte_service_address_group").addClass("error");
				$("#dte_service_address_error").html(error_message);
				$("#dte_service_address_error").show();
				return false;
				}
			}
		
		//street name
		var mail_street = $("#Mail_Address").val();
		//city
		var mail_city = $("#Mail_City").val();
		//state
		var mail_state = $("#Mail_State").val();
		//zip
		var mail_zip = $("#Mail_Zip").val();
		
		var checkbox = $("#service_mail").attr("checked")
		
		if (checkbox != 'checked')
			{
			if(mail_street == "")
				{
				var error_message = "The street number is a required field";
				$("#Mail_Address").focus();
				$("#mail_address_group").addClass("error");
				$("#mail_address_error").html(error_message);
				$("#mail_address_error").show();
				return false;
				}
			if(mail_city == "")
				{
				var error_message = "The city is required for the service address";
				$("#Mail_City").focus();
				$("#mail_address_group").addClass("error");
				$("#mail_address_error").html(error_message);
				$("#mail_address_error").show();
				return false;
				}
			if(mail_state == "")
				{
				var error_message = "The city is required for the service address";
				$("#Mail_State").focus();
				$("#mail_address_group").addClass("error");
				$("#mail_address_error").html(error_message);
				$("#mail_address_error").show();
				return false;
				}
			if(mail_zip == "")
				{
				var error_message = "A 5 digit zip code is required";
				$("#Mail_Zip").focus();
				$("#mail_address_group").addClass("error");
				$("#mail_address_error").html(error_message);
				$("#mail_address_error").show();
				return false;
				}
			if(mail_zip != "" && !mail_zip.match(numericPattern))
				{
				var error_message = "A valid 5 digit zip code must contain only numbers";
				$("#Mail_Zip").focus();
				$("#mail_address_group").addClass("error");
				$("#mail_address_error").html(error_message);
				$("#mail_address_error").show();
				return false;
				}
			}
		});
	});
